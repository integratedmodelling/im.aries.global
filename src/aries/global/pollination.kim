@documented(pollination)
namespace aries.global.pollination
	using (FLOWERING_PROBABILITY_TABLE, NESTING_PROBABILITY_TABLE) 
		from aries.global.pollination.tables;

@documented(pollination.nesting-suitability)
model occurrence of agriculture:Pollinator ecology:Nesting
	observing landcover:LandCoverType without landcover:WaterBody named landcover
	lookup (landcover) into NESTING_PROBABILITY_TABLE;

@documented(pollination.flower-availability)
model probability of ecology:Flowering
	observing landcover:LandCoverType without landcover:WaterBody named landcover
	lookup (landcover) into FLOWERING_PROBABILITY_TABLE;

@documented(pollination.insectoccurrence.landscape)	
model occurrence of agriculture:Pollinator biology:Insect caused by ecology:Landscape named landscape_suitability
	observing
	    landcover:LandCoverType named landcover_type,
		probability of ecology:Flowering named flowering_suitability,
		occurrence of agriculture:Pollinator ecology:Nesting named nesting_suitability
	set to [(landcover_type is landcover:WaterBody)? unknown : (
		     nesting_suitability * flowering_suitability)]; 

@documented(pollination.insectoccurrence.weather)
@intensive(space, time)
model occurrence of agriculture:Pollinator biology:Insect caused by earth:Weather
	observing
		earth:AtmosphericTemperature in Celsius named air_temperature,
		earth:SolarRadiation in MJ/(m^2*day) named solar_radiation
	set to [0.62 + 1.027 * air_temperature + 0.006 * solar_radiation];
	
@documented(pollination.insectoccurrence)	
model occurrence of agriculture:Pollinator biology:Insect
	observing 
		occurrence of agriculture:Pollinator biology:Insect caused by earth:Weather 
			named insect_activity_by_weather,
		occurrence of agriculture:Pollinator biology:Insect caused by ecology:Landscape
			named landscape_suitability
	set to [insect_activity_by_weather/insect_activity_by_weather.max * landscape_suitability];
		
@documented(pollination.netbenefit)	
model im:Net value of ecology:Pollination
	observing
		ratio of agriculture:Pollinated agriculture:Yield to agriculture:Yield
			named pollinated_yield,
		occurrence of agriculture:Pollinator biology:Insect 
			named pollinator_occurrence
	set to [pollinator_occurrence - pollinated_yield];
