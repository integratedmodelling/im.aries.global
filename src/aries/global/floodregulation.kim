namespace aries.global.floodregulation
	using 
 		(flow_directions_d8) from components.geoprocessing.morphometry;

model proportion of earth:PrecipitationVolume caused by earth:AtmosphericTemperature
	"Temperature effect on rainfall intensity"
	observing
		type of earth:LatitudinalRegion named latitudinal_region,
		earth:AtmosphericTemperature during (earth:Wet earth:Season) in Celsius
			named mean_wet_season_temperature,
		im:Normalized (earth:AtmosphericTemperature during (earth:Wet earth:Season)) 
			named normalized_wet_season_temperature
	lookup (latitudinal_region, mean_wet_season_temperature) into
	        region      |   mean_temperature  |     result
	    ----------------------------------------------------------------
	    earth:Polar     |         *           | [normalized_wet_season_temperature],
	    earth:Tropical  |         *           | [1 - normalized_wet_season_temperature],
	    earth:Temperate |       < 22          | [normalized_wet_season_temperature],
	    earth:Temperate |       > 22          | [1 - normalized_wet_season_temperature];

@documented(flood.twi)		    
model occurrence of earth:Region with im:Still earth:PrecipitationVolume //named topographic_wet_index
	observing 
      earth:Upstream im:Area of earth:Region in m^2 named contributing_area,
		geography:Slope in degree_angle named slope
	set to [
		def sloperadians = Math.tan((slope * 1.570796 ) / 90) 
	  	def twi = Math.log( (contributing_area+1) / Math.tan( (sloperadians+0.001) ) );
	  	return twi;
	];

@main
@documented('flood.probability')
model im:Potential proportion of earth:PrecipitationVolume causing earth:Flood
	observing
		im:Normalized earth:PrecipitationVolume named precipitation,
		im:Normalized (occurrence of earth:Region with im:Still earth:PrecipitationVolume) named water_permanence,
		proportion of earth:PrecipitationVolume caused by earth:AtmosphericTemperature named temperature_rainfall_effect
	set to [(precipitation + water_permanence + temperature_rainfall_effect)/3];

//project private model hydrology:RunoffWaterVolume with landcover:BareArea in mm
//	observing
//		hydrology:StreamOutlet,
//		hydrology:CurveNumber with landcover:BareArea named curve_number,
//		earth:PrecipitationVolume in mm,
//		flow_directions_d8
//	using im.hydrology.runoff();

//project private model hydrology:CurveNumber with landcover:BareArea named curve_number_with_bare_area
//	"Curve number model assuming there is no vegetation anywhere, using only HSG and the portion of the LUT 
//     corresponding to landcover:BareArea"
//	observing
//		landcover:LandCoverType named land_cover_type,
//		type of hydrology:HydrologicSoilGroup named hydrological_soil_group
//	lookup (land_cover_type, hydrological_soil_group, ?) into
//	
//	  land_cover_type              | hydrological_soils_group | curve_number
//	  --------------------------------------------------------------
//      landcover:ArtificialSurface  |   hydrology:SoilGroupA   |   80,
//      landcover:ArtificialSurface  |   hydrology:SoilGroupB   |   85,
//      landcover:ArtificialSurface  |   hydrology:SoilGroupC   |   90,
//      landcover:ArtificialSurface  |   hydrology:SoilGroupD   |   95,
//      landcover:ArtificialSurface  |   #                      |   87, 
//      landcover:WaterBody          |   #                      |   98,   
//      #                            |   hydrology:SoilGroupA   |   72,
//      #                            |   hydrology:SoilGroupB   |   82,
//      #                            |   hydrology:SoilGroupC   |   83,
//      #                            |   hydrology:SoilGroupD   |   87,
//      #                            |   #                      |   83;

@intensive(space)
model im:Differential hydrology:RunoffWaterVolume caused by ecology:Vegetation in mm
	observing 
		hydrology:RunoffWaterVolume in m named actual_runoff,
		hydrology:RunoffWaterVolume with landcover:BareArea in mm named runoff_without_vegetation,
		presence of earth:Stream named presence_of_stream
	set to [nodata(actual_runoff) ? unknown : (presence_of_stream ? 0 : (runoff_without_vegetation - actual_runoff)) ];

@main
@documented(flood.potential)
@intensive(space)
model im:Potential value of es:FloodRegulation
	observing 
		im:Differential hydrology:RunoffWaterVolume caused by ecology:Vegetation in mm named avoided_runoff,
		im:Potential proportion of earth:PrecipitationVolume causing earth:Flood named proneness_to_flooding
	set to [avoided_runoff * proneness_to_flooding];

@main
@documented(flood.demand)	
@intensive(space)
model ses:Demanded value of es:FloodRegulation
	observing 
		count of demography:HumanIndividual per km^2 named population_density,
		im:Potential proportion of earth:PrecipitationVolume causing earth:Flood named proneness_to_flooding
		//using gis.points.collect (select = [population_density > 10], aggregate = [population_density], aggregation= mean, radius = 2000, circular = true);
		set to [ proneness_to_flooding * population_density];
	
@main
@documented(flood.balance)
model value of es:FloodRegulation
	observing
		im:Potential value of es:FloodRegulation named flood_regulation_supply,
		ses:Demanded value of es:FloodRegulation named flood_regulation_demand
	set to [flood_regulation_supply * flood_regulation_demand],
	klab.data.normalize();
	