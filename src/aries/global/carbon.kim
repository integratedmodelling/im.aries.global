namespace aries.global.carbon
	using (VEGETATION_CARBON_TABLE) from aries.global.carbon.tables;


@documented(carbon.global.totalstorage)
model chemistry:Organic chemistry:Carbon im:Mass
	observing
		ecology:Vegetation chemistry:Carbon im:Mass named vegetation_carbon_storage,
		soil:Soil chemistry:Organic chemistry:Carbon im:Mass named soil_carbon_storage
	set to [vegetation_carbon_storage + soil_carbon_storage]; 

@documented(carbon.global.vegetation)
@intensive(space)
model ecology:Vegetation chemistry:Carbon im:Mass in t/ha
	observing
		landcover:LandCoverType without landcover:WaterBody named land_cover_type,
		presence of chemistry:Burned earth:Region named burned_land,
		type of geography:ContinentalRegion named continental_region,
		presence of im:Critical (conservation:Pristine ecology:Forest earth:Region) named frontier_forest,
		type of ecology:EcoFloristicRegion named ecofloristic_region
	lookup (land_cover_type, ecofloristic_region, continental_region, frontier_forest, burned_land, ?) 
		into VEGETATION_CARBON_TABLE; 

/**
 * Specific for these regions where burned areas and frontier forests aren't covered. 
 * TODO contextualize to area instead of using identity - this is to fill holes in a
 * batch process.
 */
@intensive(space)
model geography:ArcticRegion ecology:Vegetation chemistry:Carbon im:Mass in t/ha
	observing
		landcover:LandCoverType without landcover:WaterBody named land_cover_type,
		false as presence of chemistry:Burned earth:Region named burned_land,
		type of geography:ContinentalRegion named continental_region,
		false as presence of im:Critical (conservation:Pristine ecology:Forest earth:Region) named frontier_forest,
		type of ecology:EcoFloristicRegion named ecofloristic_region
	lookup (land_cover_type, ecofloristic_region, continental_region, frontier_forest, burned_land, ?) 
		into VEGETATION_CARBON_TABLE; 